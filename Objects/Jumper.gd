extends Area2D

signal captured
signal died

onready var trail = $Trail/Points

var velocity = Vector2(100, 0)
var jump_speed = 1500 # Default = 1000
var trail_length = 25
var target = null

func _ready():
	$Sprite.material.set_shader_param("color", Settings.theme['player_body'])
	var trail_color = Settings.theme['player_trail']
	trail.gradient.set_color(1, trail_color)
	trail_color.a = 0
	trail.gradient.set_color(0, trail_color)

func _unhandled_input(event):
	if target and event is InputEventScreenTouch and event.is_pressed():
		jump()

func jump():
	target.implode()
	target = null
	velocity = transform.x * jump_speed
	if Settings.enable_sound:
		$Jump.play()
		print("jump")

func _on_Jumper_area_entered(area):
	target = area
	velocity = Vector2.ZERO
	emit_signal("captured", target)
	if Settings.enable_sound:
		$Capture.play()
		print("capture")

func _physics_process(delta):
	# Control trail
	if trail.points.size() > trail_length:
		trail.remove_point(0)
	trail.add_point(position)
	# Control movement
	if target:
		transform = target.orbit_position.global_transform
	else:
		position += velocity * delta

func die():
	target = null
	queue_free()

func _on_VisibilityNotifier2D_screen_exited():
	if !target:
		die()
		emit_signal("died")
