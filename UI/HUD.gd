extends CanvasLayer

var score = 0

func show_message(text):
	if $MessageAnimation.is_playing():
		yield($MessageAnimation, "animation_finished")
	$Message.text = text
	$MessageAnimation.play("Show")

func hide():
	$BonusBox/Bonus.text = "1x"
	$ScoreBox/HBoxContainer/Score.text = "0"
	$BonusBox.hide()
	$ScoreBox.hide()

func show():
	$BonusBox.show()
	$ScoreBox.show()

func update_score(current_score, new_score):
	if new_score > 0:
		$ScoreTween.interpolate_property(self, "score", current_score, new_score, 0.25, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$ScoreTween.start()
		$ScoreAnimation.play("Score")

func update_bonus(value):
	$BonusBox/Bonus.text = str(value) + "x"
	if value > 1:
		$BonusAnimation.play("Bonus")

func _ready():
	$Message.rect_pivot_offset = $Message.rect_size / 2

func _on_ScoreTween_tween_step(object, key, elapsed, value):
	$ScoreBox/HBoxContainer/Score.text = str(int(value))
